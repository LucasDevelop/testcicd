fastlane documentation
----

# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```sh
xcode-select --install
```

For _fastlane_ installation instructions, see [Installing _fastlane_](https://docs.fastlane.tools/#installing-fastlane)

# Available Actions

## Android

### android build_apk_uat

```sh
[bundle exec] fastlane android build_apk_uat
```

send msg to ding talk.

### android upload_apk_zealot

```sh
[bundle exec] fastlane android upload_apk_zealot
```

upload apk file to zealot service.

### android upload_apk_pgyer

```sh
[bundle exec] fastlane android upload_apk_pgyer
```

upload apk file to pgyer service.

### android send_msg_ding_talk

```sh
[bundle exec] fastlane android send_msg_ding_talk
```

send msg to ding talk.

----

This README.md is auto-generated and will be re-generated every time [_fastlane_](https://fastlane.tools) is run.

More information about _fastlane_ can be found on [fastlane.tools](https://fastlane.tools).

The documentation of _fastlane_ can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
